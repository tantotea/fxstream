# Stream of Instruments

This project creates 20 charts in a scrollable page. Each chart contains a stream of rates of an instrument, returned from OANDA stream API call (https://developer.oanda.com).

The project is written in Swift 4.2 for iOS, using the following libraries:
- Alamofire (https://github.com/Alamofire/Alamofire), for the api calls
- Charts (https://github.com/danielgindi/Charts), for showing the charts

## Testing

Tests were carried out on iOS 12.1 using iPhone XR simulator and iPhone 6 device. 

Unit tests were done when converting response from OANDA API into objects (using Swift 4's JSONDecoder). 

When the market seems closed, i.e. the stream returns no data, set IS_MOCK=true in APIHandler.swift to use random numbers as stream.
