//
//  ViewController.swift
//  FxRealTime
//
//  Created by Johannes Dwiartanto on 15/11/18.
//  Copyright © 2018 Johannes Dwiartanto. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var fxViewCtrl: FxViewController = FxViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        self.fxViewCtrl = sb.instantiateViewController(withIdentifier: "FxViewController") as! FxViewController
        self.navigationController?.pushViewController(self.fxViewCtrl, animated: false)
    }

}

