//
//  FxData.swift
//  FxRealTime
//
//  Created by Johannes Dwiartanto on 15/11/18.
//  Copyright © 2018 Johannes Dwiartanto. All rights reserved.
//
//  Response sample structure
//  {
//     "type":"PRICE",
//     "time":"1542337388.572854340",
//     "bids":[
//         {
//             "price":"1.37584",
//             "liquidity":10000000
//         }
//     ],
//     "asks":[
//         {
//             "price":"1.37602",
//             "liquidity":10000000
//         }
//     ],
//     "closeoutBid":"1.37584",
//     "closeoutAsk":"1.37602",
//     "status":"tradeable",
//     "tradeable":true,
//     "instrument":"USD_SGD"
//  }


import UIKit

struct FxData {
    let instrument: String
    let bid: Double
    let ask: Double
    let time: Double //UNIX format
    
    enum CodingKeys: String, CodingKey {
        case instrument
        case bid = "closeoutBid"
        case ask = "closeoutAsk"
        case time
    }
}

extension FxData: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        let bidStr: String = try values.decode(String.self, forKey: .bid)
        bid = Double(bidStr) ?? 0.0
        
        let askStr: String = try values.decode(String.self, forKey: .ask)
        ask = Double(askStr) ?? 0.0
        
        instrument = try values.decode(String.self, forKey: .instrument)
        
        let timeStr: String = try values.decode(String.self, forKey: .time)
        time = Double(timeStr) ?? 0.0
    }
}

