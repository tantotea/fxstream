//
//  Instrument.swift
//  FxRealTime
//
//  Created by Johannes Dwiartanto on 17/11/18.
//  Copyright © 2018 Johannes Dwiartanto. All rights reserved.
//

import UIKit

struct InstrumentsData: Decodable {
    let instruments: [Instrument]
}

struct Instrument: Decodable {
    let name: String
    let type: String
    let displayName: String
}
