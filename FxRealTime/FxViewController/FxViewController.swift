//
//  FxViewController.swift
//  FxRealTime
//
//  Created by Johannes Dwiartanto on 15/11/18.
//  Copyright © 2018 Johannes Dwiartanto. All rights reserved.
//

import UIKit


let MAX_INSTR: Int = 20  //max instruments 20 by OANDA stream
let MAX_CHART_POINTS: Int = 15  //avoid too crowded chart

class FxViewController: UITableViewController {

    var apiHandler = APIHandler()
    var instruments = [Instrument]()
    var fxList = [[FxData]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.hidesBackButton =  true
        self.navigationItem.title = "Stream of Instruments"
        
        self.populateWithEmptyItems()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loadInstruments(completion: {(instCsv: String) in
            self.loadFxData(instrumentsCsv: instCsv)
        })
    }

    // MARK: - TableView Delegates
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fxList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FxChartCell") as! FxTableViewCell
        
        cell.setChartLabel(self.instruments[indexPath.row].displayName)
        
        let fxDatas: [FxData] = self.fxList[indexPath.row]
        cell.updateChart(fxDatas)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.size.height / 4.0;
    }
    
    // MARK: - Data
    
    // Load instruments allowed, with max
    func loadInstruments( completion: @escaping(String) -> Void) {
        self.apiHandler.getInstruments(completion: { (instsData: InstrumentsData?) in
            let allInsts = instsData!.instruments
            
            var instArr = [String]()
            for (idx, inst) in allInsts.enumerated() {
                if idx >= MAX_INSTR {
                    break
                }
                else {
                    instArr.append(inst.name)
                    self.instruments[idx] = inst
                }
            }
            
            let instCsv: String = instArr.joined(separator: ",")
            //print("instCsv \(instCsv)")
            
            completion(instCsv)
        })
    }
    
    // Load stream of fx data of instruments, then update the tableView
    func loadFxData(instrumentsCsv: String) {
        self.apiHandler.getStreamFx(instruments: instrumentsCsv, completion: { (fxData: FxData?) in
            //print("fxData \(String(describing: fxData))")
            
            if let instName = fxData?.instrument {
                var found: Bool = false
                
                for (idx, inst) in self.instruments.enumerated() { //20 is not many
                    if inst.name == instName {
                        if let instTime = fxData?.time, let lastTime = self.fxList[idx].last?.time {
                        
                            let seconds: Double = instTime - lastTime
                            if (seconds > 5.0) { //avoid too close between one to another
                                self.fxList[idx].append(fxData!)
                                self.fxList[idx] = Array( self.fxList[idx].suffix(MAX_CHART_POINTS) ) //last few only
                            
                                found = true
                                break;
                            }
                        }
                    }
                }
                if found {
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
        })
    }
    
    // Populate array with empty items
    func populateWithEmptyItems() {
        self.instruments.reserveCapacity(MAX_INSTR)
        self.fxList.reserveCapacity(MAX_INSTR)
        for _ in (0..<MAX_INSTR) {
            self.instruments.append(Instrument(name:"", type:"", displayName:""))
            self.fxList.append([FxData(instrument:"", bid:0, ask:0, time:0)])
        }
    }
}
