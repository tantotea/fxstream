//
//  FxTableViewCell.swift
//  FxRealTime
//
//  Created by Johannes Dwiartanto on 18/11/18.
//  Copyright © 2018 Johannes Dwiartanto. All rights reserved.
//

import UIKit
import Charts

class FxTableViewCell: UITableViewCell, ChartViewDelegate {

    @IBOutlet weak var chartView: LineChartView!
    var chartLabel = String()
    var values = [ChartDataEntry]()
    
    // MARK: - UITableViewCell
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.initChart()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Charts lib api
    
    func initChart() {
        self.chartView.delegate = self
        self.chartView.chartDescription?.enabled = false
        self.chartView.dragEnabled = true
        self.chartView.setScaleEnabled(true)
        self.chartView.pinchZoomEnabled = true
        self.chartView.zoomToCenter(scaleX: 1.0, scaleY: 1.0)
        self.chartView.xAxis.gridLineDashLengths = [10, 10]
        self.chartView.xAxis.gridLineDashPhase = 0
        self.chartView.xAxis.drawLabelsEnabled = false
        self.chartView.rightAxis.enabled = false
        self.chartView.legend.form = .line
        self.chartView.legend.font = .boldSystemFont(ofSize: 12)
        self.chartView.animate(xAxisDuration: 2.5)
        let marker = BalloonMarker(color: UIColor.darkGray,
                                   font: .systemFont(ofSize: 12),
                                   textColor: .white,
                                   insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = self.chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        self.chartView.marker = marker
    }
    
    func setChartLabel(_ label: String) {
        self.chartLabel = label
    }
    
    func updateChart(_ fxDatas: [FxData]) {
        self.values = [ChartDataEntry]()
        for fxData in fxDatas {
            if fxData.instrument != "" {
                let avgRate: Double = (fxData.bid + fxData.ask) / 2
                self.values.append( ChartDataEntry(x:fxData.time, y:avgRate) )
            }
        }
        
        let set = LineChartDataSet(values: self.values, label: self.chartLabel)
        set.drawIconsEnabled = false
        set.lineDashLengths = [5, 2.5]
        set.highlightLineDashLengths = [5, 2.5]
        set.setColor(.black)
        set.setCircleColor(.black)
        set.lineWidth = 1
        set.circleRadius = 3
        set.drawCircleHoleEnabled = false
        set.drawValuesEnabled = false
        set.valueFont = .systemFont(ofSize: 11)
        //set.valueFormatter = DefaultValueFormatter(decimals: 2)
        set.formLineDashLengths = [5, 2.5]
        set.formLineWidth = 1
        set.formSize = 15
        
        let gradientColors = [ChartColorTemplates.colorFromString("#00ff0000").cgColor,
                              ChartColorTemplates.colorFromString("#ffff0000").cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        set.fillAlpha = 1
        set.fill = Fill(linearGradient: gradient, angle: 90)
        set.drawFilledEnabled = true
        
        let data = LineChartData(dataSet: set)
        self.chartView.data = data
    }
}
