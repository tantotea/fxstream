//
//  APIHandler.swift
//  FxRealTime
//
//  Created by Johannes Dwiartanto on 15/11/18.
//  Copyright © 2018 Johannes Dwiartanto. All rights reserved.
//

import UIKit
import Alamofire

// Whether using mock random data for the FxData stream
var IS_MOCK = false

class APIHandler: NSObject {
    
    let apiToken: String = "01a2b497a9ef4439b017132a4c6b9640-384aefca894848ea5a5201c58d82a7e5"
    let accountId: String = "101-003-9798147-001"
    let baseStreamUrl: String = "https://stream-fxpractice.oanda.com/v3"
    let baseApiUrl: String = "https://api-fxpractice.oanda.com/v3"
    
    //MOCK VARS (OANDA seems closed on Saturday/Sunday)
    var mockTime: Int = 1
    var mockInstruments = [String]()
    var mockCount: Int = 0
    //END OF MOCK VARS
    
    // Get all instruments allowed
    //
    // - Parameters:
    //   - completion: Instruments data
    func getInstruments(completion: @escaping(InstrumentsData?) -> Void) {
        let urlStr: String = self.baseApiUrl + "/accounts/" + self.accountId + "/instruments"
        guard let url = URL(string: urlStr) else {
            completion(nil)
            return
        }
        let headers = [
            "Authorization": "Bearer " + self.apiToken,
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        Alamofire.request(url,
                          method: .get,
                          headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                    case .success:
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: response.result.value!, options: .prettyPrinted)
                            let instsData = try JSONDecoder().decode(InstrumentsData.self, from: jsonData)
                            
                            //TEST
                            for inst in instsData.instruments {
                                self.mockInstruments.append(inst.name)
                            }
                            
                            completion(instsData)
                        } catch let error as NSError  {
                            print(error)
                            completion(nil)
                        }
                    
                    case .failure(let error):
                        print(error)
                }
        }
    }
    
    // Get a stream of Foreign Exchange rates
    //
    // - Parameters:
    //   - instruments: list of Fx, comma separated
    //   - completion: streaming Fx data
    func getStreamFx(instruments: String, completion: @escaping (FxData?) -> Void) {
        
        if IS_MOCK == false
        {
            let urlStr: String = self.baseStreamUrl + "/accounts/" + self.accountId + "/pricing/stream"
            guard let url = URL(string:urlStr) else {
                completion(nil)
                return
            }
            let headers = [
                "Authorization": "Bearer " + self.apiToken,
                "Content-Type": "application/json",
                "Accept-Datetime-Format": "UNIX",
                "Accept": "application/json",
                "Connection": "keep-alive"
            ]
            let parameters = [
                "instruments": instruments
            ]
            
            Alamofire.request(url,
                              method: .get,
                              parameters: parameters,
                              headers: headers)
                .validate()
                .stream { data in
                    do {
                        //print("data \(data)")
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .iso8601
                        let fxData = try decoder.decode(FxData.self, from: data)
                        completion(fxData)
                    }
                    catch let error as NSError {
                        //print(error)
                    }
            }
        }
        else
        {
            //---MOCK (OANDA seems closed on Saturday/Sunday)
            weak var timer: Timer?
            timer?.invalidate()
            timer = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true) { _ in
                self.mockTime = self.mockTime + 1
                if self.mockCount > 19 {
                    self.mockCount = 0;
                }
                let fxData = FxData(
                    instrument: self.mockInstruments[self.mockCount],
                    bid: Double.random(in: 0..<0.5) + 1.0,
                    ask: Double.random(in: 0..<0.5) + 1.5,
                    time: Double(self.mockTime)
                )
                self.mockCount = self.mockCount + 1
                completion(fxData)
            }
            //---END OF MOCK
        }
    }
}
