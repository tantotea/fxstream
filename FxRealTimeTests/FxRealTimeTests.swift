//
//  FxRealTimeTests.swift
//  FxRealTimeTests
//
//  Created by Johannes Dwiartanto on 15/11/18.
//  Copyright © 2018 Johannes Dwiartanto. All rights reserved.
//

import XCTest


@testable import FxRealTime

class FxRealTimeTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetStreamFx_fxDataIsExpected() {
        let jsonString = "{\"type\":\"PRICE\",\"time\":\"1542337388.572854340\",\"bids\":[{\"price\":\"1.37584\",\"liquidity\":10000000}],\"asks\":[{\"price\":\"1.37602\",\"liquidity\":10000000}],\"closeoutBid\":\"1.37584\",\"closeoutAsk\":\"1.37602\",\"status\":\"tradeable\",\"tradeable\":true,\"instrument\":\"USD_SGD\"}"
        
        let jsonData = jsonString.data(using: .utf8)!
        
        do {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            let fxData = try decoder.decode(FxData.self, from: jsonData)
            XCTAssertEqual(fxData.instrument, "USD_SGD", ".instrument is wrong")
            XCTAssertEqual(fxData.bid, 1.37584, ".bid is wrong")
            XCTAssertEqual(fxData.ask, 1.37602, ".ask is wrong")
            XCTAssertEqual(fxData.time, 1542337388.572854340, ".time is wrong")
        } catch let error as NSError {
            print(error)
        }
    }

    func testGetInstruments_instrumentsDataIsExpected() {
        let jsonString = "{\"instruments\":[{\"name\":\"XAG_CAD\",\"type\":\"METAL\",\"displayName\":\"Silver/CAD\",\"pipLocation\":-4,\"displayPrecision\":5,\"tradeUnitsPrecision\":0,\"minimumTradeSize\":\"1\",\"maximumTrailingStopDistance\":\"1.00000\",\"minimumTrailingStopDistance\":\"0.00050\",\"maximumPositionSize\":\"0\",\"maximumOrderUnits\":\"1000000\",\"marginRate\":\"0.02\"},{\"name\":\"USB30Y_USD\",\"type\":\"CFD\",\"displayName\":\"US T-Bond\",\"pipLocation\":-2,\"displayPrecision\":3,\"tradeUnitsPrecision\":0,\"minimumTradeSize\":\"1\",\"maximumTrailingStopDistance\":\"100.000\",\"minimumTrailingStopDistance\":\"0.050\",\"maximumPositionSize\":\"0\",\"maximumOrderUnits\":\"60000\",\"marginRate\":\"0.2\"},{\"name\":\"GBP_USD\",\"type\":\"CURRENCY\",\"displayName\":\"GBP/USD\",\"pipLocation\":-4,\"displayPrecision\":5,\"tradeUnitsPrecision\":0,\"minimumTradeSize\":\"1\",\"maximumTrailingStopDistance\":\"1.00000\",\"minimumTrailingStopDistance\":\"0.00050\",\"maximumPositionSize\":\"0\",\"maximumOrderUnits\":\"100000000\",\"marginRate\":\"0.02\"}],\"lastTransactionID\":\"3\"}"
        
        let jsonData = jsonString.data(using: .utf8)!
    
        do {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            let insData = try decoder.decode(InstrumentsData.self, from: jsonData)
            XCTAssertEqual(insData.instruments.count, 3, ".instruments.count is wrong")
            XCTAssertEqual(insData.instruments[0].type, "METAL", ".instruments[0].type is wrong")
            XCTAssertEqual(insData.instruments[2].name, "GBP_USD", ".instruments[2].name is wrong")
            XCTAssertEqual(insData.instruments[2].type, "CURRENCY", ".instruments[2].type is wrong")
            XCTAssertEqual(insData.instruments[2].displayName, "GBP/USD", ".instruments[2].displayName is wrong")
        } catch let error as NSError {
            print(error)
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
